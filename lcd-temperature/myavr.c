#define F_CPU 3686400UL
#include <stdio.h>
#include <compat/deprecated.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/twi.h>
#include <util/delay.h>

#define REG_LM75_TEMP 0b00000000
#define REG_LM75_CONFIG 0b00000001
#define REG_LM75_HYST 0b00000010
#define REG_LM75_SET 0b00000011

#ifndef TWI_READ
#define TWI_READ 0b00000001
#endif
#ifndef TWI_WRITE
#define TWI_WRITE 0b00000000
#endif

#define I2C_DDR         DDRC
#define I2C_DDR_REG_BIT DDC7
#define I2C_PORT        PORTC
#define SCL             PC5
#define SDA             PC4

void twiStart()
{
   uint8_t x=TWCR;
   x&=TWEN|TWIE;    // nur Beibehalten von Enable und InterruptJ/N 
   TWCR=x|TWINT|TWSTA;
   // warten bis fertig
   while( !(TWCR & TWINT))
   {}
}

void twiStop()
{
   uint8_t x=TWCR;
   x&=TWEN|TWIE;    // nur Beibehalten von Enable und InterruptJ/N 
   TWCR=x|TWINT|TWSTO;
}

void twiWriteByte(uint8_t data, uint8_t ackn)
{
   TWDR=data;      // Daten bereitlegen
   // Befehl zusammenstellen
   uint8_t x=TWCR;
   x&=TWEN|TWIE;      //nur Beibehalten von Enable und InterruptJ/N 
   x|=TWINT;
   if(ackn)   
      x|=TWEA;   // evt. TWEA setzen, für Datenanforderung
   TWCR=x;         // senden
   // warten bis fertig
   while( !(TWCR & TWINT))
   {}
}

int twiReadByte(uint8_t ackn)
{
   // Befehl zusammenstellen
   uint8_t x=TWCR;
   x&=TWEN|TWIE;      //nur Beibehalten von Enable und InterruptJ/N 
   x|=TWINT;
   if(ackn)   
      x|=TWEA;   // evt. TWEA setzen, für Datenanforderung
   TWCR=x;         // senden
   // warten bis fertig
   while( !(TWCR & TWINT))
   {}
   return TWDR;
}

void lm75SetRegister(uint8_t twiAdr,uint8_t reg, uint8_t highData, uint8_t lowData)
{
   twiStart();
   //Adresse senden
   twiWriteByte((twiAdr&0xfe)|TWI_WRITE,0);   // kein ACKN
   //Register adressieren
   twiWriteByte(reg,0);            // kein ACKN
   //Wert schreiben
   twiWriteByte(highData,0);         // kein ACKN
   //Wert schreiben
   twiWriteByte(lowData,0);         // kein ACKN
   // Stopp
   twiStop();

}

void lm75Init(uint8_t twiAdr, uint8_t compTemp=0,uint8_t compTempHalf=0)
{
   // Config = NoShutdown,ComperatorMode,ActiveLow,FaultQueue=0
   lm75SetRegister(twiAdr,REG_LM75_CONFIG,0,0);
   // keine Hysterese
   lm75SetRegister(twiAdr,REG_LM75_HYST,0,0);
   // Comperator-PIN schaltet z.B. bei = 0x1C = 28   C
   lm75SetRegister(twiAdr,REG_LM75_SET,compTemp,compTempHalf);
}

uint8_t lm75GetTempDecGrad(uint8_t twiAdr)
{
   // TWI starten
   twiStart();
   // Adresse senden
   twiWriteByte((twiAdr&0xfe)|TWI_WRITE,0);   // kein ACKN
   // Temperatur adressieren
   twiWriteByte(REG_LM75_TEMP,0);         // kein ACKN

   //--- Lesezyklus
   // TWI re-starten
   twiStart();
   // Adresse senden
   twiWriteByte((twiAdr&0xfe)|TWI_READ,1);      // mit ACKN = acknowledge = TWI-Datenanforderung
   // lesen
   uint8_t   highData=twiReadByte(1);      // mit ACKN = acknowledge = TWI-Datenanforderung
   uint8_t   lowData=twiReadByte(0);         // kein ACKN

   // Stopp
   twiStop();
   return (highData<<1)|(lowData>>7);
}

// #define SLAVE_ADDRESS (0x01)

// uint8_t adc_value; // the value to send
// 
// ISR(TWI_vect) {
//   // react on TWI status and handle different cases
//   uint8_t status = TWSR & 0xFC; // mask-out the prescaler bits
// 
//   switch (status) {
//     case TW_ST_SLA_ACK:   // own SLA+R received, acknowledge sent
//       TWDR = adc_value;
//       TWCR &= ~((1 << TWSTO) | (1 << TWEA));
//       break;
// 
//     case TW_ST_LAST_DATA: // last byte transmitted ACK received
//       TWCR |= (1 << TWEA); // set TWEA to enter slave mode
//       break;
//   }
// 
//   TWCR |= (1 << TWINT);  // set TWINT -> activate TWI hardware
// }

void lcd_send(char data) {
  char rs = PORTD;

  rs &= 4;

  char tmp = data;

  tmp &= 0xF0;
  tmp |= rs;

  PORTD = tmp;

  sbi(PORTD, 3);
  cbi(PORTD, 3);

  tmp = data;
  tmp &= 0x0F;
  tmp *= 16;
  tmp |= rs;

  PORTD = tmp;

  sbi(PORTD, 3);
  cbi(PORTD, 3);

  _delay_ms(1);
}

void lcd_cmd(char cmd) {
  cbi(PORTD, 2);
  lcd_send(cmd);
}

void lcd_enable() {
  sbi(PORTB, 2);
  _delay_us(1);
  cbi(PORTB, 2);
}

void lcd_clear() {
  lcd_cmd(0x01);
  _delay_ms(2);
}

void lcd_home() {
  lcd_cmd(0x02);
  _delay_ms(2);
}

void lcd_on() {
  lcd_cmd(0x0E);
}

void lcd_off() {
  lcd_cmd(0x08);
}

void lcd_goto(int row, int col) {
  row--;
  row &= 0x01;
  row *= 0x40;
  col--;
  col &= 0x0f;

  lcd_cmd(row | col | 0x80);
}

void lcd_softreset() {
  PORTD = 0x30;
  sbi(PORTD, 3);
  cbi(PORTD, 3);
  _delay_ms(5);
  PORTD = 0x30;
  sbi(PORTD, 3);
  cbi(PORTD, 3);
  _delay_us(150);
  PORTD = 0x30;
  sbi(PORTD, 3);
  cbi(PORTD, 3);
  _delay_ms(5);
}

void lcd_init() {
  DDRD = 0x0ff;
  PORTD = 0;

  _delay_ms(50);

  lcd_softreset();

  PORTD = 0x20;

  sbi(PORTD, 3);
  cbi(PORTD, 3);

  _delay_ms(5);

  lcd_cmd(0x28);
  lcd_off();
  lcd_clear();
  lcd_cmd(0x06);
  lcd_on();
  lcd_clear();
}

void lcd_write(char text) {
  sbi(PORTD, 2);
  lcd_send(text);
}

void lcd_write(char const * text) {
  while (text[0] != 0) {
    lcd_write(text[0]);
    text++;
  }
}

int main() {
  // // Enable global interrupt for TWI
  // sei();

  // // set slave address to 0x01, ignore general call
  // TWAR = (SLAVE_ADDRESS << 1) | 0x00;
  // // TWI-Enable , TWI Interrupt Enable
  // TWCR |= (1 << TWEA) | (1 << TWEN) | (1 << TWIE);

  // // ADC setup
  // ADCSRA |= (1 << ADEN);
  // ADMUX  |= ((1 << REFS1) | (1 << REFS0)); // select internal reference
  // ADMUX  |= 3; // select channel 5 (pin ADC5)

  _delay_ms(200);

  lcd_init();
  lcd_goto(1, 1);

  lcd_write("YESSS!");

  short i = 0;

  while (1) {
    // ADCSRA |= (1 << ADSC);        // start single measurement
    // while (ADCSRA & (1 << ADSC));  // wait until measurement done

    // adc_value = ADCL;
    // adc_value >>= 2;          // drop 2 least significant bits
    // adc_value |= (ADCH << 6); // add two most significant bits

    char out[] = "";
    sprintf(out, "%d - %i", lm75GetTempDecGrad(0x90), i++);
    lcd_clear();
    lcd_write(out);
    _delay_ms(50);
  }

  return 0;
}
