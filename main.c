#define F_CPU 3686400UL
#include <compat/deprecated.h>
#include <avr/io.h>
#include <util/delay.h>

void lcd_send(char data) {
	char rs = PORTD;

	rs &= 4;

	char tmp = data;

	tmp &= 0xF0;
	tmp |= rs;

	PORTD = tmp;

	sbi(PORTD, 3);
	cbi(PORTD, 3);

	tmp = data;
	tmp &= 0x0F;
	tmp *= 16;
	tmp |= rs;

	PORTD = tmp;

	sbi(PORTD, 3);
	cbi(PORTD, 3);

	_delay_ms(1);
}

void lcd_cmd(char cmd) {
	cbi(PORTD, 2);
	lcd_send(cmd);
}

void lcd_enable() {
	sbi(PORTB, 2);
	_delay_us(1);
	cbi(PORTB, 2);
}

void lcd_clear() {
	lcd_cmd(0x01);
	_delay_ms(2);
}

void lcd_home() {
	lcd_cmd(0x02);
	_delay_ms(2);
}

void lcd_on() {
	lcd_cmd(0x0E);
}

void lcd_off() {
	lcd_cmd(0x08);
}

void lcd_goto(int row, int col) {
	row--;
	row &= 0x01;
	row *= 0x40;
	col--;
	col &= 0x0f;

	lcd_cmd(row | col | 0x80);
}

void lcd_softreset() {
	PORTD = 0x30;
	sbi(PORTD, 3);
	cbi(PORTD, 3);
	_delay_ms(5);
	PORTD = 0x30;
	sbi(PORTD, 3);
	cbi(PORTD, 3);
	_delay_us(150);
	PORTD = 0x30;
	sbi(PORTD, 3);
	cbi(PORTD, 3);
	_delay_ms(5);
}

void lcd_init() {
	DDRD = 0x0ff;
	PORTD = 0;

	_delay_ms(50);

	lcd_softreset();

	PORTD = 0x20;

	sbi(PORTD, 3);
	cbi(PORTD, 3);

	_delay_ms(5);

	lcd_cmd(0x28);
	lcd_off();
	lcd_clear();
	lcd_cmd(0x06);
	lcd_on();
	// lcd_clear();
}

void lcd_write_char(char text) {
	sbi(PORTD, 2);
	lcd_send(text);
}

void lcd_write_string(char* text) {
	while (text[0] != 0) {
		lcd_write_char(text[0]);
		text++;
	}
}

int main() {
	_delay_ms(200);

	lcd_init();
	lcd_goto(1, 1);

	lcd_write_string("YESSS!");

	while (1) { /* do nothing */ }

	return 0;
}
